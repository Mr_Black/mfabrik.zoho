from core import Connection, decode_json

class Creator(Connection):

	def get_service_name(self):
		return "ZohoCreator"

	def get_records(self, application_name, view_name):

		self.ensure_opened()
		
		url = "https://creator.zoho.com"
		url += "/api"
		url += "/json"
		url += "/" + application_name
		url += "/view"
		url += "/" + view_name + "/"

		response = self.do_call(url, { "raw" : "true" })
		data = decode_json(response)

		return data
